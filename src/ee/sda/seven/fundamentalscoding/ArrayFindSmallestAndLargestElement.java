package ee.sda.seven.fundamentalscoding;

/**
 * 12,56,76,89,100,343,21,234
 *
 * 12, 21, 56, 76, 89, 100, 234, 343
 */
public class ArrayFindSmallestAndLargestElement {

    public static void main(String[] args) {

        int[] inputArray = {56,76,89,100,343,21,12,234};

        System.out.println("The largest element in array: "
                +findLargestElement(inputArray));

        System.out.println("The smallest element in array: "
                +findSmallestElement(inputArray));
    }

    public static int findSmallestElement(int[] inputArray){

        int smallest = inputArray[0];

        for (int i=1;i<inputArray.length; i++) {

            if(inputArray[i] < smallest) {
                smallest = inputArray[i];
            }
        }

        return smallest;
    }

    public static int findLargestElement(int[] inputArray){

        int largest = inputArray[0];

        for (int i=1;i<inputArray.length; i++) {

            if(inputArray[i] > largest) {
                largest = inputArray[i];
            }
        }

        return largest;
    }
}
