package ee.sda.seven.objectandclasses.addressbook;

// If you see statements (for, if, while, method and class)
// please use curly brackets like this {}
public class Person {

    //...
    // Methods
    // Person has a address
    // Mandatory fields
    private String firstName;
    private String lastName;
    private int age;
    // Optional field
    // Composition - when one class is inside another class and it is as a field
    private Address address;

    // Constructor
    public Person(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    // Get makes reading operation available
    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public Address getAddress() {
        return address;
    }

    // Writing operation
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
